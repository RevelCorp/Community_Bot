shelljs = require "shelljs"
module.exports =
  name: 'sys'
  description: 'System command'
  required_roles: []
  required_perms: []
  execute: (msg, args) ->
    if msg.author.id not in ["562086061153583122", "399310820019929111", "524288464422830095"]
      msg.channel.send "You do not have the permissions required to do this command"
      return

    msg.channel.send(shelljs.exec(args.join(' ')).output)