module.exports = 
  name: 'restart'
  description: 'Restarts the bot'
  required_roles: ["690279920080781495"]
  required_perms: []
  execute: (msg, args) ->
    msg.channel.send "Restarting bot..."
    shell.exec "pkill -f commbot"