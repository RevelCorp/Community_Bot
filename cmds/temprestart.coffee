shell=require 'shelljs'
module.exports = 
  name: 'temprestart'
  description: 'Restarts the bot'
  required_roles: []
  required_perms: []
  execute: (msg, args) ->
    msg.channel.send "Restarting bot..."
    shell.exec "pkill -f commbot"